use std::{
	env,
	path::PathBuf,
};

use libbpf_cargo::SkeletonBuilder;

fn main() {
	let mut out = PathBuf::from(env::var_os("OUT_DIR").unwrap());
	out.push("sec.skel.rs");
	SkeletonBuilder::new()
		.source("src/bpf/sec.bpf.c")
		.clang_args("-Wall -Werror")
		.build_and_generate(&out)
		.unwrap();
	println!("cargo:rerun-if-changed={}", "src/bpf/sec.bpf.c");
}
