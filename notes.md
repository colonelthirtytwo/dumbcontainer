systemd in container
---

https://systemd.io/CONTAINER_INTERFACE/
https://www.freedesktop.org/wiki/Software/systemd/writing-vm-managers/
https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface/

netlink (managing net interfaces)
---------------------------------

http://ifeanyi.co/posts/linux-namespaces-part-4/

Tor Browser Bundle using system Tor
------------------------------------

https://gitlab.torproject.org/legacy/trac/-/wikis/TorBrowserBundleSAQ

```
# Using a system-installed Tor process with Tor Browser:
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The Tor ControlPort password should be given inside double quotes, inside
# single quotes, i.e. if the ControlPort password is “secret” (without
# curly quotes) then we must set the environment variable *exactly* like
# this:
#
# TOR_CONTROL_PASSWD='"secret"'
#
# Yes, the variable MUST be double-quoted, then single-quoted, exactly as
# shown. This is used by TorButton and Tor Launcher to authenticate to Tor's
# ControlPort, and is necessary for using TB with a system-installed Tor.
#
# Additionally, if using a system-installed Tor, the following about:config
# options should be set (values in <> mean they are the value taken from your
# torrc):
#
# SETTING NAME                            VALUE
# network.security.ports.banned           [...],<SocksPort>,<ControlPort>
# network.proxy.socks                     127.0.0.1
# network.proxy.socks_port                <SocksPort>
# extensions.torbutton.inserted_button    true
# extensions.torbutton.launch_warning     false
# extensions.torbutton.loglevel           2
# extensions.torbutton.logmethod          0
# extensions.torlauncher.control_port      <ControlPort>
# extensions.torlauncher.loglevel          2
# extensions.torlauncher.logmethod         0
# extensions.torlauncher.prompt_at_startup false
# extensions.torlauncher.start_tor         false
#
# where the '[...]' in the banned_ports option means "leave anything that was
# already in the preference alone, just append the things specified after it".
```

xpra
----

    xpra start --mmap=/home/col/xpratest/ --socket-dir=/home/col/xpratest/ --bind=/home/col/xpratest/ --border=red --windows=yes --pulseaudio=yes --file-transfer=off --open-files=off --forward-xdg-open=off --daemon=no --mdns=no --dbus-proxy=no --html=off --webcam=no --sharing=no --encoding=rgb --auth=allow --attach=no --exit-with-children=yes --exit-with-client=yes '--start=fish'
