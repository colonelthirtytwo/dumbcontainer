A (rather unsuccessful) attempt at creating a privacy container with host-inaccessible encrypted storage and network filters to only allow Tor traffic.

The most interesting and complex part, and the one I want to document, is the host-inaccessible encrypted storage part, because it's hard and will probably never be 100% secure. Doing some restrictions, though, is still useful to prevent innocent applications like thumbnailers and indexers from leaking the container's contents.

Containers, as opposed to virtual machines or alternate partitions, are useful because of their comparatively lighter weight. You can spin up, shutdown, and use a container without having to reboot, and since the host and container share the same kernel and resources, they are more responsive - they are effectively running "natively".

But sandboxing the main namespace from the container's is a problem, since the main namespace and system is more trusted. The main challenges are as follows:

1. The container shares the same kernel, therefore the kernel must be trusted.
2. Root on the main namespace has permissions to access and modify the container.
3. Devices are not able to be separated via namespaces. In particular, the device-mapper dm-crypt plaintext virtual device can't be restricted to a namespace, and is visible to all namespaces that have /dev mounted.

We don't try to solve issue 1; the issue is fundamental to how containers work, and if an attacker can run kernel code, there's very little they cannot do anyway. Issue 2 is also tricky for the same reason, since the superuser has similar system access. However there is a way to restrict the root user: Linux Security Modules. In particular, eBPF security modules can be easily installed and removed by the container manager, and don't need to be recompiled on every kernel change. This project's eBPF filter, found in src/bpf/sec.bpf.c, restricts access to the namespace (via its /proc/ inodes) as well as the plaintext dm-crypt virtual device. This keeps out most of the usual ways to enter the namespace and access the data.

There are shortcomings though. There is a race condition between configuring device-mapper and creating the plaintext device and the eBFP being configured to protect it. The actual filesystem isn't protected - only the namespace containing it is. And, of course, this all depends on the host properly setting everything up, and the host can be altered to expose holes.

A full LSM can provide more in-depth protections at the cost of being more invasive to deploy and needing to be kept up-to-date with the kernel.
