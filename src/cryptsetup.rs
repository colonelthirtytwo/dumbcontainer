use std::{
	collections::HashMap,
	ffi::OsString,
	fmt::Write as _,
	io::Write,
	os::unix::prelude::{
		AsRawFd,
		OsStringExt,
	},
	path::{
		Path,
		PathBuf,
	},
};

use anyhow::Context;
use devicemapper::{
	DevId,
	Device,
	DeviceInfo,
	DmCookie,
	DmFlags,
	DmName,
	DmOptions,
	DmUuid,
	DM,
};
use nix::{
	fcntl::OFlag,
	libc::dev_t,
	sys::stat::{
		makedev,
		Mode,
	},
};
use rand::Rng;
use serde::Deserialize;

use crate::secbpf::SecBpf;

/// Manager for the dm-crypt device
pub struct ContainerStorage<'bpf> {
	dm: DM,
	id: String,
	lodev: Option<PathBuf>,
	info: DeviceInfo,
	bpf: Option<&'bpf mut SecBpf>,
	deleted: bool,
}
impl<'bpf> ContainerStorage<'bpf> {
	/// Creates an uninitialized device-mapper entry.
	///
	/// Creates a device but doesn't set up any tables to make it functional.
	pub fn create_uninitialized(
		name: &str,
		id: impl Into<String>,
		mut bpf: Option<&'bpf mut crate::secbpf::SecBpf>,
	) -> anyhow::Result<Self> {
		let id = id.into();
		let dm = DM::new().context("While opening device mapper")?;
		let mut rng = rand::thread_rng();
		let dm_major = get_dm_major()?;

		if let Some(bpf) = bpf.as_mut() {
			bpf.pin(&id)?;
		}

		let mut attempts = 0;
		let info = loop {
			let used = get_used_dm_minors(dm_major)?;
			let minor = loop {
				let minor = rng.gen_range(100u32..255u32);
				if used.contains(&minor) {
					continue;
				} else {
					break minor;
				}
			};
			// TODO: check for minor number exhaustion.

			trace!(
				"Creating dm device {}:{}, attempt {}",
				dm_major,
				minor,
				attempts + 1
			);
			bpf.as_mut()
				.map(|bpf| bpf.set_plaintext_dev(Some(makedev(dm_major as u64, minor as u64))));
			let res = dm.device_create(
				DmName::new(name).context("Invalid dm name")?,
				Some(DmUuid::new(&id).unwrap()),
				DmOptions::default()
					.set_flags(DmFlags::DM_PERSISTENT_DEV)
					.set_device(Device {
						major: dm_major,
						minor,
					})
					.set_cookie(cookie_udev_ignore()),
			);
			match res {
				Ok(info) => break info,
				Err(e) if attempts >= 6 => {
					bpf.as_mut().map(|bpf| bpf.set_plaintext_dev(None));
					return Err(e).context("While creating dm device");
				}
				Err(e) => {
					bpf.as_mut().map(|bpf| bpf.set_plaintext_dev(None));
					warn!("Could not creeate dm device {}:{}, trying another minor number. Error was: {}",
						dm_major, minor, e);
					attempts += 1;
				}
			}
		};
		trace!("Created dm node. Device num: {:?}", info.device());

		Ok(Self {
			dm,
			id,
			lodev: None,
			info,
			bpf,
			deleted: false,
		})
	}

	/// Gets the dev_t number for the device
	pub fn devnum(&self) -> dev_t {
		self.info.device().into()
	}

	pub fn mknod(&self, path: &Path, perms: Mode) -> anyhow::Result<()> {
		nix::sys::stat::mknod(path, nix::sys::stat::SFlag::S_IFBLK, perms, self.devnum())
			.map_err(Into::into)
	}

	/// Deletes the device.
	///
	/// Also called on drop, but this lets you get the result.
	pub fn delete(mut self) -> anyhow::Result<()> {
		self.delete_impl()
	}

	fn delete_impl(&mut self) -> anyhow::Result<()> {
		self.deleted = true;
		self.dm
			.device_remove(
				&DevId::Name(self.info.name().unwrap()),
				devicemapper::DmOptions::default().set_flags(DmFlags::DM_DEFERRED_REMOVE),
			)
			.context("While deleting dm device")?;

		if let Some(bpf) = self.bpf.as_mut() {
			bpf.unpin(&self.id)?;
		}

		if let Some(lodev) = self.lodev.as_ref() {
			loteardown(lodev).context("While deleting lo device")?;
		}
		Ok(())
	}

	pub fn decrypt(
		&mut self,
		encrypted_path: &Path,
		passphrase: Option<&str>,
	) -> anyhow::Result<()> {
		let md = query_luks_metadata(encrypted_path)?;
		trace!("LUKS metadata: {:?}", md);
		if md.segments.len() != 1 {
			anyhow::bail!("LUKS setups with multiple segments not yet implemented");
		}
		let segment = md.segments.into_iter().next().unwrap().1;
		if segment.type_ != "crypt" {
			anyhow::bail!(
				"LUKS setups with non-'crypt' segments not yet implemented. Type: {:?}",
				segment.type_
			);
		}
		if let SegmentSize::Value(_) = segment.size {
			anyhow::bail!("LUKS setups with non-dynamic segments not yet implemented");
		}
		if segment.offset % 512 != 0 {
			anyhow::bail!(
				"Segment offset of {} is not divisible by 512",
				segment.offset
			);
		}
		if segment.iv_tweak != 0 {
			// TODO: what does this do?
			anyhow::bail!("LUKS non-zero iv_tweak not yet implemented");
		}
		let blockdev_info = BlockDevInfo::query(encrypted_path)?;
		trace!("Block device info: {:?}", blockdev_info);

		let (dev_id, size_bytes) = match blockdev_info {
			BlockDevInfo::BlockDev { dev_id, size_bytes } => {
				if size_bytes % 512 != 0 {
					anyhow::bail!(
						"Block device size of {:?} was not a multiple of 512",
						size_bytes
					);
				}
				(dev_id, size_bytes)
			}
			BlockDevInfo::DiskImage => {
				let lopath = losetup(encrypted_path)
					.context("While setting up loopback device for encrypted image")?;
				self.lodev = Some(lopath.clone());
				let loinfo = BlockDevInfo::query(&lopath).context("While gettng lo dev info")?;
				match loinfo {
					BlockDevInfo::BlockDev { dev_id, size_bytes } => {
						debug!("lo dev id: {:?}", dev_id);
						(dev_id, size_bytes)
					}
					BlockDevInfo::DiskImage => {
						anyhow::bail!("lo dev is somehow a normal file");
					}
				}
			}
		};

		let offset_sectors = segment.offset / 512;
		let len_sectors = size_bytes
			.checked_sub(segment.offset)
			.map(|v| v / 512)
			.ok_or_else(|| {
				anyhow::anyhow!(
					"Block device size of {} less than encrypted segment offset {}",
					size_bytes,
					segment.offset
				)
			})?;

		let key = query_volume_key(encrypted_path, passphrase)?;
		let key_hex = hex::encode(&key);

		let mut addtl_opts = String::new();
		let mut num_addtl_opts = 0;

		if segment.sector_size != 512 {
			write!(&mut addtl_opts, " sector_size:{}", segment.sector_size).unwrap();
			num_addtl_opts += 1;
		}

		self.dm
			.table_load(
				&DevId::Name(self.info.name().unwrap()),
				&[(
					0,
					len_sectors,
					"crypt".into(),
					format!(
						"{} {} 0 {}:{} {} {}{}",
						segment.encryption,
						key_hex,
						nix::sys::stat::major(dev_id.0),
						nix::sys::stat::minor(dev_id.0),
						offset_sectors,
						num_addtl_opts,
						addtl_opts,
					),
				)],
				DmOptions::default(),
			)
			.context("While loading dm-crypt table")?;

		self.dm
			.device_suspend(
				&DevId::Name(self.info.name().unwrap()),
				DmOptions::default().set_cookie(cookie_udev_ignore()),
			)
			.context("While activating dm-crypt device")?;
		Ok(())
	}
}
impl<'bpf> std::ops::Drop for ContainerStorage<'bpf> {
	fn drop(&mut self) {
		if !self.deleted {
			if let Err(e) = self.delete_impl() {
				error!("{:#}", e);
			}
		}
	}
}

fn cookie_udev_ignore() -> DmCookie {
	DmCookie::DM_UDEV_DISABLE_DM_RULES_FLAG
		| DmCookie::DM_UDEV_DISABLE_SUBSYSTEM_RULES_FLAG
		| DmCookie::DM_UDEV_DISABLE_DISK_RULES_FLAG
		| DmCookie::DM_UDEV_DISABLE_OTHER_RULES_FLAG
}

fn query_volume_key(blk: &Path, passphrase: Option<&str>) -> anyhow::Result<Vec<u8>> {
	let mut cmd = std::process::Command::new("cryptsetup");
	cmd.args(&["luksDump", "--dump-volume-key", "-q"])
		.stdout(std::process::Stdio::piped())
		.stderr(std::process::Stdio::inherit())
		.stdin(std::process::Stdio::inherit());

	if passphrase.is_some() {
		cmd.args(&["--key-file", "-"]);
		cmd.stdin(std::process::Stdio::piped());
	}

	cmd.arg("--").arg(blk);

	let mut child = cmd.spawn().context("While executing cryptsetup")?;

	if let Some(passphrase) = passphrase {
		let mut stdin = child.stdin.take().unwrap();
		stdin
			.write_all(passphrase.as_bytes())
			.context("While writing passphrase")?;
		std::mem::drop(stdin);
	}

	let output = child
		.wait_with_output()
		.context("While waiting for cryptsetup")?;

	if !output.status.success() {
		anyhow::bail!("cryptsetup exited with code {}", output.status);
	}

	let output_str =
		std::str::from_utf8(&output.stdout).context("While parsing cryptsetup output")?;
	let mut mk = None;
	let mut lines = output_str.lines().peekable();
	while let Some(line) = lines.next() {
		if let Some(mut mk_line) = check_strip_prefix(line, "MK dump:") {
			let mut mk_wip = vec![];
			loop {
				let mk_line_hex = mk_line.trim().replace(|ch: char| ch.is_whitespace(), "");
				let mk_line_bytes = hex::decode(&mk_line_hex)
					.context("While parsing key line (not showing for safety)")?;
				mk_wip.extend_from_slice(&mk_line_bytes);
				if lines
					.peek()
					.and_then(|l| l.chars().next().map(|ch| ch.is_whitespace()))
					.unwrap_or(false)
				{
					mk_line = lines.next().unwrap();
				} else {
					break;
				}
			}
			mk = Some(mk_wip);
		}
	}

	let mk = mk.ok_or_else(|| anyhow::anyhow!("cryptsetup didn't report key"))?;
	Ok(mk)
}

fn check_strip_prefix<'a>(haystack: &'a str, needle: &str) -> Option<&'a str> {
	if haystack.starts_with(needle) {
		Some(&haystack[needle.len()..])
	} else {
		None
	}
}

fn query_luks_metadata(blk: &Path) -> anyhow::Result<LuksJsonMetadata> {
	let mut cmd = std::process::Command::new("cryptsetup");
	cmd.stdout(std::process::Stdio::piped())
		.stderr(std::process::Stdio::inherit())
		.stdin(std::process::Stdio::inherit())
		.args(&["luksDump", "--dump-json-metadata", "-q", "--"])
		.arg(blk);
	let output = cmd.output().context("While running cryptsetup")?;
	if !output.status.success() {
		anyhow::bail!("cryptsetup exited with code {}", output.status);
	}
	serde_json::from_slice(&output.stdout).context("While deserializing cryptsetup output")
}

#[derive(serde::Deserialize, Debug)]
struct LuksJsonMetadata {
	pub segments: HashMap<String, LuksSegment>,
}

#[derive(serde::Deserialize, Debug)]
struct LuksSegment {
	#[serde(rename = "type")]
	pub type_: String,
	#[serde(deserialize_with = "deser_string_u64")]
	pub offset: u64,
	pub size: SegmentSize,
	#[serde(deserialize_with = "deser_string_u64")]
	pub iv_tweak: u64,
	pub encryption: String,
	pub sector_size: u64,
}

fn deser_string_u64<'de, D: serde::de::Deserializer<'de>>(de: D) -> Result<u64, D::Error> {
	use serde::de::Error as _;
	let s = String::deserialize(de)?;
	s.parse::<u64>().map_err(|e| D::Error::custom(e))
}

#[derive(Debug, Clone, Copy)]
enum SegmentSize {
	Value(u64),
	Dynamic,
}
impl<'de> serde::de::Deserialize<'de> for SegmentSize {
	fn deserialize<D>(de: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error as _;
		let s = String::deserialize(de)?;
		if s == "dynamic" {
			return Ok(Self::Dynamic);
		}
		s.parse::<u64>()
			.map(Self::Value)
			.map_err(|e| D::Error::custom(e))
	}
}

#[derive(Clone, Debug)]
enum BlockDevInfo {
	BlockDev { dev_id: DevT, size_bytes: u64 },
	DiskImage,
}
impl BlockDevInfo {
	pub fn query(path: &Path) -> anyhow::Result<Self> {
		let fd = nix::fcntl::open(path, OFlag::O_RDONLY, Mode::empty())
			.map(|fd| filedescriptor::OwnedHandle::new(fd))
			.with_context(|| format!("While opening {:?}", path))?;

		let stat_info = nix::sys::stat::fstat(fd.as_raw_fd()).context("While fstat'ing device")?;
		match stat_info.st_mode & nix::libc::S_IFMT {
			nix::libc::S_IFBLK => {
				if stat_info.st_rdev == 0 {
					anyhow::bail!("Supposed block device had st_rdev == 0");
				}
				let mut size_bytes = 0u64;
				let res = unsafe {
					blkgetsize64(fd.as_raw_fd(), &mut size_bytes as *mut _ as *mut usize)
				};
				res.context("While executing blkgetsize64 ioctl")?;

				Ok(Self::BlockDev {
					size_bytes,
					dev_id: DevT(stat_info.st_rdev),
				})
			}
			nix::libc::S_IFREG => Ok(Self::DiskImage),
			_ => anyhow::bail!("Encrypted disk path is not a block device or regular file"),
		}
	}
}

#[derive(Clone, Copy)]
struct DevT(pub dev_t);
impl std::fmt::Debug for DevT {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_tuple("DevT")
			.field(&nix::sys::stat::major(self.0))
			.field(&nix::sys::stat::minor(self.0))
			.finish()
	}
}

// NOTE: size_t in _IOR macro but actually reads 64 bytes...
nix::ioctl_read!(blkgetsize64, 0x12, 114, usize);

fn losetup(disk_img: &Path) -> anyhow::Result<PathBuf> {
	// TODO: set up ourselves
	let res = std::process::Command::new("losetup")
		.arg("--show")
		.arg("--find")
		.arg("--")
		.arg(disk_img)
		.stdout(std::process::Stdio::piped())
		.stderr(std::process::Stdio::inherit())
		.output()
		.context("While executing losetup")?;
	if !res.status.success() {
		anyhow::bail!("losetup exited with code {}", res.status);
	}
	let mut out = res.stdout;
	while out
		.last()
		.copied()
		.map(|c| c.is_ascii_whitespace())
		.unwrap_or(false)
	{
		out.pop();
	}

	let path = PathBuf::from(OsString::from_vec(out));
	Ok(path)
}
fn loteardown(lodev: &Path) -> anyhow::Result<()> {
	let res = std::process::Command::new("losetup")
		.arg("--detach")
		.arg(lodev)
		.spawn()
		.context("While executing losetup")?
		.wait()
		.context("While waiting for losetup")?;
	if !res.success() {
		anyhow::bail!("losetup exited with code {}", res);
	}
	Ok(())
}

fn get_dm_major() -> anyhow::Result<u32> {
	let devices =
		std::fs::read_to_string("/proc/devices").context("While reading /proc/devices")?;
	let block_section = devices
		.split_once("Block devices:")
		.ok_or_else(|| {
			anyhow::anyhow!("Could not parse /proc/devices: could not find block devices section")
		})?
		.1;
	for line in block_section.lines() {
		let line = line.trim();
		if line.is_empty() {
			continue;
		}
		let (num, desc) = match line.split_once(' ') {
			Some(v) => v,
			None => anyhow::bail!(
				"Could not parse /proc/devices: could not parse line {:?}",
				line
			),
		};
		let num = num
			.parse::<u32>()
			.with_context(|| format!("While parsing /proc/devices line {:?}", line))?;
		let desc = desc.trim();

		if desc == "device-mapper" {
			return Ok(num);
		}
	}
	anyhow::bail!("Could not find device-mapper entry in /proc/devices")
}

fn get_used_dm_minors(dm_major: u32) -> anyhow::Result<Vec<u32>> {
	let mut it = std::fs::read_dir("/sys/dev/block").context("While reading /sys/dev/block")?;
	let mut vec = vec![];
	while let Some(res) = it.next() {
		let e = res.context("While reading /sys/dev/block")?;
		let n = e.file_name();
		let n = n.to_string_lossy();
		let (major, minor) = match n.split_once(':') {
			Some(v) => v,
			None => continue,
		};
		let major = match major.parse::<u32>() {
			Ok(v) => v,
			Err(_) => continue,
		};
		let minor = match minor.parse::<u32>() {
			Ok(v) => v,
			Err(_) => continue,
		};

		if major != dm_major {
			continue;
		}

		vec.push(minor);
	}

	Ok(vec)
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn root_needed_encryption_setup() {
		let file = tempfile::NamedTempFile::new().unwrap();
		nix::unistd::ftruncate(file.as_raw_fd(), 1024 * 1024 * 25).unwrap();
		let mut res = std::process::Command::new("cryptsetup")
			.args(&[
				"luksFormat",
				"--type",
				"luks2",
				"--key-file",
				"-",
				"--batch-mode",
				"--",
			])
			.arg(file.path())
			.stdin(std::process::Stdio::piped())
			.spawn()
			.unwrap();
		let mut stdin = res.stdin.take().unwrap();
		stdin.write_all(b"asdf").unwrap();
		std::mem::drop(stdin);
		let res = res.wait().unwrap();
		assert!(res.success(), "{:?}", res);

		let mut cs = ContainerStorage::create_uninitialized(
			"dumbcontainer-test",
			"dumbcontainer-test",
			None,
		)
		.unwrap();
		assert_ne!(cs.devnum(), 0);
		cs.decrypt(file.path(), Some("asdf")).unwrap();
		cs.delete().unwrap();
	}
}
