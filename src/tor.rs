use std::{
	fs::Permissions,
	os::unix::prelude::PermissionsExt,
};

use anyhow::Context;
use nix::mount::MsFlags;
use rand::RngCore;
use sha1::Digest;

use crate::{
	pidfd::Pidfd,
	util::{
		cargs,
		CmdExt,
	},
};

const TORRC_TEMPLATE: &str = r#"
DataDirectory /tmp/dumbcontainer-mount/tor-data
ControlPort unix:/tmp/dumbcontainer-bridge/tor/control.sock RelaxDirModeCheck GroupWritable WorldWritable
SocksPort unix:/tmp/dumbcontainer-bridge/tor/proxy.sock KeepAliveIsolateSOCKSAuth GroupWritable WorldWritable
HashedControlPassword %PASSWORD%
NoExec 1
__OwningControllerProcess %PID%
Sandbox 1
User tor
Log info file /tmp/dumbcontainer-bridge/tor/log.txt
"#;

pub fn start_tor() -> anyhow::Result<Pidfd> {
	let password = crate::util::generate_random_id();
	let hasheded_password = tor_hash_password(&password);

	let torrc = TORRC_TEMPLATE
		.replace("%PASSWORD%", &hasheded_password)
		.replace("%PID%", &nix::unistd::getpid().as_raw().to_string());
	std::fs::write("/tmp/dumbcontainer-torrc", torrc.as_bytes())
		.context("While writing /tmp/dumbcontainer-torrc")?;

	std::process::Command::new("chown")
		.args(&["-R", "tor:tor", "--", "/tmp/dumbcontainer-mount/tor-data"])
		.spawn_wait_check("chmod tor data")?;
	std::fs::set_permissions(
		"/tmp/dumbcontainer-bridge/tor",
		Permissions::from_mode(0o755),
	)
	.context("While changing mode of tor bridge dir")?;
	std::process::Command::new("chown")
		.args(&["tor:tor", "--", "/tmp/dumbcontainer-bridge/tor"])
		.spawn_wait_check("chmod tor bridge")?;

	let mut pidfd = 0;
	let mut clone = clone3::Clone3::default();
	clone
		.flag_newns()
		.flag_vfork()
		.flag_pidfd(&mut pidfd)
		.exit_signal(nix::libc::SIGCHLD as u64);

	let pid = unsafe { clone.call() }.context("While cloning")?;
	if pid == 0 {
		crate::util::cloned_child(child);
	}

	Ok(Pidfd::from_fd(pidfd))
}

fn child() -> anyhow::Result<()> {
	unsafe { nix::libc::prctl(nix::libc::PR_SET_PDEATHSIG, nix::libc::SIGTERM) };

	nix::mount::mount(
		None as Option<&str>,
		"/",
		None as Option<&str>,
		MsFlags::MS_SLAVE | MsFlags::MS_REC,
		None as Option<&str>,
	)
	.context("While setting mount propagation")?;

	// TODO: restrict tor

	let args = cargs!["tor", "--torrc-file", "/tmp/dumbcontainer-torrc", "--quiet"];
	let env: &[&std::ffi::CStr] = &[];
	nix::unistd::execvpe(args[0], &args, env).context("While executing tor")?;
	unreachable!()
}

fn tor_hash_password(pw: &str) -> String {
	// S2K, RFC 2440
	// Would just use `tor --hash-password` but it whines when ran as root and tries (and fails) to open the data directory
	// as anything else. So just do it ourselves.
	const SPECIFIER: u8 = 96;
	const COUNT: usize = (16 + (SPECIFIER as usize & 15)) << ((SPECIFIER as usize >> 4) + 6);

	let mut secret = Vec::with_capacity(8 + pw.len());
	secret.resize(8, 0u8);
	rand::rngs::OsRng.fill_bytes(&mut secret);
	secret.extend_from_slice(pw.as_bytes());

	let mut sha = sha1::Sha1::default();

	let mut count = COUNT;
	while count > 0 {
		sha.update(&secret[..secret.len().min(count)]);
		count = count.saturating_sub(secret.len());
	}

	let hash = sha.finalize();
	format!(
		"16:{}{:02X}{}",
		hex::encode_upper(&secret[..8]),
		SPECIFIER,
		hex::encode_upper(hash)
	)
}
