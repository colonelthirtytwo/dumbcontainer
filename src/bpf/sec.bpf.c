#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include <bpf/bpf_core_read.h>
#include <errno.h>

struct {
	__uint(type, BPF_MAP_TYPE_CGROUP_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
	__uint(max_entries, 1);
} disk_authorized_cgroup_map SEC(".maps");

dev_t restricted_namespaces_dev;
struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__uint(key_size, sizeof(ino_t));
	__uint(value_size, 1);
	__uint(max_entries, 32);
} restricted_namespaces_inodes_map SEC(".maps");

dev_t plaintext_dev;

// bpf_printk
// super_block: https://elixir.bootlin.com/linux/v5.19/source/include/linux/fs.h#L1434
// task_struct: https://elixir.bootlin.com/linux/v5.19/A/ident/task_struct

SEC("lsm/sb_kern_mount")
int BPF_PROG(handle_mount, struct super_block* sb) {
	dev_t dev = 0;
	bpf_core_read(&dev, sizeof(dev_t), &sb->s_dev);

	long is_under_cgroup = bpf_current_task_under_cgroup(&disk_authorized_cgroup_map, 0);

	if(plaintext_dev != 0 && dev == plaintext_dev && is_under_cgroup <= 0) {
		return -EPERM;
	}

	return 0;
}

SEC("lsm/inode_permission")
int BPF_PROG(handle_inode_perms, struct inode *inode, int mask) {
	long is_under_cgroup = bpf_current_task_under_cgroup(&disk_authorized_cgroup_map, 0);
	if(is_under_cgroup <= 0) {
		// Not in guest cgroup
		dev_t rdev = 0;
		bpf_core_read(&rdev, sizeof(dev_t), &inode->i_rdev);
		if(plaintext_dev != 0 && rdev == plaintext_dev) {
			// Trying to open the plaintext dev
			return -EPERM;
		}

		ino_t ino = 0;
		bpf_core_read(&ino, sizeof(ino_t), &inode->i_ino);
		struct super_block *sb = NULL;
		bpf_core_read(&sb, sizeof(struct super_block *), &inode->i_sb);
		dev_t dev = 0;
		bpf_core_read(&dev, sizeof(dev_t), &sb->s_dev);

		if(
			restricted_namespaces_dev != 0 &&
			dev == restricted_namespaces_dev &&
			bpf_map_lookup_elem(&restricted_namespaces_inodes_map, &ino) != NULL
		) {
			// Trying to open the namespace handles
			return -EPERM;
		}
	}

	return 0;
}

char _license[] SEC("license") = "GPL";
