use std::{
	fmt::Write as _,
	path::PathBuf,
};

use anyhow::Context;
use dbus::arg::Variant;
use filedescriptor::OwnedHandle;
use nix::{
	fcntl::OFlag,
	sys::stat::Mode,
};

mod bindings_systemd_manager;
mod bindings_systemd_scope;

use self::{
	bindings_systemd_manager::OrgFreedesktopSystemd1Manager,
	bindings_systemd_scope::OrgFreedesktopSystemd1Unit,
};
use crate::dbus::bindings_systemd_scope::OrgFreedesktopSystemd1Scope;

pub struct SystemD {
	dbus: dbus::blocking::LocalConnection,
}
impl SystemD {
	pub fn new() -> anyhow::Result<Self> {
		let dbus =
			dbus::blocking::LocalConnection::new_system().context("While connecting to dbus")?;
		Ok(Self { dbus })
	}

	fn sys_proxy(&self) -> dbus::blocking::Proxy<&dbus::blocking::LocalConnection> {
		self.dbus.with_proxy(
			"org.freedesktop.systemd1",
			"/org/freedesktop/systemd1",
			std::time::Duration::from_secs(5),
		)
	}

	pub fn create_scope(
		&self,
		name: &str,
		kill_on_drop: bool,
		pids: Vec<u32>,
	) -> anyhow::Result<Scope> {
		let sys_proxy = self.sys_proxy();
		let _job_path = sys_proxy
			.start_transient_unit(
				name,
				"fail",
				vec![
					("Description", vs("dumbcontainer instance")),
					("Delegate", v(true)),
					("PIDs", v(pids)),
				],
				vec![],
			)
			.context("While running StartTransientUnit")?;

		// TODO: wait for job. figure out how to do that

		// bus_label_escape in systemd src
		let mut unit_path = String::with_capacity(31 + name.len() * 3);
		unit_path.push_str("/org/freedesktop/systemd1/unit/");
		for (i, b) in name.as_bytes().iter().copied().enumerate() {
			if !b.is_ascii_alphanumeric() || (i == 0 && !b.is_ascii_alphabetic()) {
				write!(&mut unit_path, "_{:02X}", b).unwrap();
			} else {
				unit_path.push(b as char);
			}
		}

		let unit_proxy = self.dbus.with_proxy(
			"org.freedesktop.systemd1",
			unit_path,
			std::time::Duration::from_secs(5),
		);
		let cgroup_path = OrgFreedesktopSystemd1Scope::control_group(&unit_proxy)
			.context("While getting scope ControlGroup")?;
		Ok(Scope {
			unit_proxy,
			_unit_name: name.into(),
			cgroup_path,
			kill_on_drop,
		})
	}
}

pub struct Scope<'a> {
	unit_proxy: dbus::blocking::Proxy<'a, &'a dbus::blocking::LocalConnection>,
	_unit_name: String,
	cgroup_path: String,
	kill_on_drop: bool,
}
impl<'a> Scope<'a> {
	pub fn _cgroup_path(&self) -> &str {
		&self.cgroup_path
	}

	fn sub_cgroup_path(&self, name: &str) -> PathBuf {
		let mut path = PathBuf::from(format!("/sys/fs/cgroup{}", self.cgroup_path));
		path.push(name);
		path
	}

	pub fn open(&self) -> anyhow::Result<OwnedHandle> {
		let path = self.sub_cgroup_path("");
		nix::fcntl::open(
			&path,
			OFlag::O_RDONLY | OFlag::O_DIRECTORY | OFlag::O_CLOEXEC,
			Mode::empty(),
		)
		.map(|fd| OwnedHandle::new(fd))
		.with_context(|| format!("While opening cgroup dir {:?}", path))
	}
}
impl<'a> std::ops::Drop for Scope<'a> {
	fn drop(&mut self) {
		if !self.kill_on_drop {
			return;
		}
		if let Err(e) = self.unit_proxy.stop("replace") {
			if e.name() != Some("org.freedesktop.systemd1.NoSuchUnit") {
				error!("{:#}", anyhow::anyhow!(e).context("While stopping scope"));
			}
		}
	}
}

fn v<'a, I: dbus::arg::RefArg + 'a>(s: I) -> Variant<Box<dyn dbus::arg::RefArg + 'a>> {
	Variant(Box::new(s))
}
fn vs<I: Into<String>>(s: I) -> Variant<Box<dyn dbus::arg::RefArg>> {
	v(s.into())
}
