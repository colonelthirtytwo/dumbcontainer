use std::os::unix::prelude::{
	AsRawFd,
	RawFd,
};

use anyhow::Context;
use filedescriptor::OwnedHandle;
use nix::sys::wait::WaitStatus;

pub struct Pidfd(OwnedHandle);
impl Pidfd {
	pub fn from_fd(fd: RawFd) -> Self {
		Self(OwnedHandle::new(fd))
	}

	pub fn signal(&self, signal: i32) -> nix::Result<()> {
		// TODO: use safe kill binding when it supports SIGRTMIN.
		// SIGRTMIN+3 tells systemd to shut down
		let i = unsafe {
			sc::syscall4(
				sc::nr::PIDFD_SEND_SIGNAL,
				self.0.as_raw_fd() as usize,
				signal as usize,
				0,
				0,
			)
		} as i32;
		if i < 0 {
			Err(nix::errno::from_i32(-i))
		} else {
			Ok(())
		}
	}

	pub fn wait(&self) -> nix::Result<WaitStatus> {
		loop {
			match nix::sys::wait::waitid(
				nix::sys::wait::Id::PIDFd(self.0.as_raw_fd().as_raw_fd()),
				nix::sys::wait::WaitPidFlag::WEXITED | nix::sys::wait::WaitPidFlag::__WALL,
			) {
				Err(nix::errno::Errno::EINTR) => continue,
				res => return res,
			}
		}
	}

	pub fn wait_checked(&self, desc: &str) -> anyhow::Result<()> {
		let out = self
			.wait()
			.with_context(|| format!("While waiting for {}", desc))?;
		match out {
			WaitStatus::Exited(_, 0) => Ok(()),
			WaitStatus::Exited(_, res) => {
				anyhow::bail!("Process {} exited with code {}", desc, res)
			}
			WaitStatus::Signaled(_, sig, _) => {
				anyhow::bail!("Process {} exited with signal {:?}", desc, sig)
			}
			ws => anyhow::bail!("Unexpected result from waiting for {}: {:?}", desc, ws),
		}
	}
}
impl AsRawFd for Pidfd {
	fn as_raw_fd(&self) -> RawFd {
		self.0.as_raw_fd()
	}
}
