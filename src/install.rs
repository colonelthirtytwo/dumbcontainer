use std::io::Write;

use anyhow::Context;
use nix::mount::MsFlags;

use crate::util::ProcExt;

pub(crate) fn setup(args: crate::InstallArgs) -> anyhow::Result<()> {
	println!(
		"Warning! All data on {:?} will be lost!",
		args.encrypted_partition
	);

	let disk_password = loop {
		let p1 = rpassword::prompt_password("Enter desired encryption passphrase: ")
			.context("While prompting for password")?;
		if p1 == "" {
			eprintln!("Password cannot be empty");
			continue;
		}

		let p2 = rpassword::prompt_password("Verify encryption passphrase: ")
			.context("While prompting for password")?;
		if p1 != p2 {
			eprintln!("Passwords do not match, try again.");
			continue;
		}

		break p1;
	};

	let user_password = loop {
		let p1 = rpassword::prompt_password(
			"Enter desired user password (leave blank to use encryption password):",
		)
		.context("While promting for password")?;
		if p1 == "" {
			break disk_password.clone();
		}

		let p2 = rpassword::prompt_password("Verify user passphrase: ")
			.context("While prompting for password")?;
		if p1 != p2 {
			eprintln!("Passwords do not match, try again.");
			continue;
		}

		break p1;
	};

	let id = crate::util::generate_random_id();
	crate::util::unshare_mounts(MsFlags::MS_SLAVE)?;
	nix::mount::mount(
		Some("tmpfs"),
		"/tmp",
		Some("tmpfs"),
		MsFlags::empty(),
		None as Option<&str>,
	)
	.context("While making tmpfs")?;

	info!("Setting up device encryption");

	// Pass phrase through a file, so that cryptsetup can ask questions.
	// Should be safe since our tmpfs is private.
	std::fs::write("/tmp/password", disk_password.as_bytes())
		.context("While writing password file")?;

	std::process::Command::new("cryptsetup")
		.args(&[
			"luksFormat",
			"--type",
			"luks2",
			"--key-file",
			"/tmp/password",
			"--",
		])
		.arg(&args.encrypted_partition)
		.spawn()
		.context("While executing cryptsetup luksFormat")?
		.wait_checked("cryptsetup luksFormat")?;
	let _ = std::fs::remove_file("/tmp/password");

	crate::mount::with_mounted_container(
		&args.encrypted_partition,
		&id,
		Some(&disk_password),
		|| {
			info!("Formatting");
			std::process::Command::new("mkfs.ext4")
				.args(&["--", "/tmp/dumbcontainer-plaintext-dev"])
				.spawn()
				.context("While executing mkfs.ext4")?
				.wait_checked("mkfs.ext4")?;
			Ok(())
		},
		|| {
			std::fs::create_dir("/tmp/dumbcontainer-mount/tor-data")
				.context("While creating tor data directory")?;
			std::fs::create_dir("/tmp/dumbcontainer-mount/root")
				.context("While creating root directory")?;

			info!("Pacstrapping");

			std::process::Command::new("pacstrap")
				.args(&["--", "/tmp/dumbcontainer-mount/root"])
				.args(&[
					"base",
					// System utilities
					"sudo",
					"nano",
					// xpra x11 forwarding and addons
					"xpra",
					"python-dbus",
					"gst-python",
					// tor and addons
					"tor",
					"torsocks",
					"torbrowser-launcher",
					// socat to forward tor ports to the tor socket
					"socat",
				])
				.spawn()
				.context("While executing pacstrap")?
				.wait_checked("pacstrap")?;

			info!("Configuring container");
			// We don't need internet access now, so get rid of it while we mess with the container
			nix::sched::unshare(nix::sched::CloneFlags::CLONE_NEWNET)
				.context("While unsharing network namespace")?;

			nix::mount::mount(
				Some("/tmp/dumbcontainer-mount/root"),
				"/tmp/dumbcontainer-mount/root",
				None as Option<&str>,
				MsFlags::MS_BIND,
				None as Option<&str>,
			)
			.context("While bind-mounting root")?;

			// Make a user
			std::process::Command::new("arch-chroot")
				.args(&[
					"/tmp/dumbcontainer-mount/root",
					"useradd",
					"-m",
					"-G",
					"wheel",
					"-s",
					"/bin/bash",
					"amnesia",
				])
				.spawn()
				.context("While spawning arch-chroot useradd")?
				.wait_checked("arch-chroot useradd")?;

			trace!("Setting password");
			let mut proc = std::process::Command::new("arch-chroot")
				.args(&["/tmp/dumbcontainer-mount/root", "chpasswd"])
				.stdin(std::process::Stdio::piped())
				.spawn()
				.context("While spawning arch-chroot passwd")?;
			proc.stdin
				.as_mut()
				.unwrap()
				.write_all(format!("amnesia:{}\n", user_password).as_bytes())
				.context("While writing stdin to chpasswd")?;
			std::mem::drop(proc.stdin.take());
			proc.wait_checked("arch-chroot chpasswd")?;

			trace!("Setting up sudoers file");
			{
				let mut f = std::fs::File::options()
					.append(true)
					.open("/tmp/dumbcontainer-mount/root/etc/sudoers")
					.context("While opening sudoers")?;
				f.write_all(b"\n%wheel ALL=(ALL:ALL) ALL\n")
					.context("While writing sudoers")?;
			}

			trace!("Adding systemd jobs for tor port-to-socket forwarding");
			std::fs::write(
				"/tmp/dumbcontainer-mount/root/etc/systemd/system/socat-tor-proxy.service",
				SOCAT_TOR_PROXY.as_bytes(),
			)
			.context("While writing etc/systemd/system/socat-tor-proxy.service")?;
			std::fs::write(
				"/tmp/dumbcontainer-mount/root/etc/systemd/system/socat-tor-ctrl.service",
				SOCAT_TOR_CTRL.as_bytes(),
			)
			.context("While writing etc/systemd/system/socat-tor-ctrl.service")?;

			trace!("Adding systemd config for auto login");
			std::fs::create_dir(
				"/tmp/dumbcontainer-mount/root/etc/systemd/system/console-getty.service.d/",
			)
			.context("While creating etc/systemd/system/console-getty.service.d/")?;
			std::fs::write(
				"/tmp/dumbcontainer-mount/root/etc/systemd/system/console-getty.service.d/autologin.conf",
				AUTOLOGIN.as_bytes(),
			)
			.context("While writing etc/systemd/system/console-getty.service.d/autologin.conf")?;

			println!("All set up. Username is `anmesia`.");
			Ok(())
		},
	)
}

const SOCAT_TOR_PROXY: &str = r#"
[Unit]
Description=Forward Port 9050 to host Tor socket
[Service]
ExecStart=socat TCP-LISTEN:9050,fork UNIX-CONNECT:/run/host/dumbcontainer-bridge/tor/proxy.sock
Type=simple
Restart=always
User=nobody
Group=nobody

NoNewPrivileges=true
ProtectSystem=true
ProtectHome=true
PrivateDevices=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
"#;

const SOCAT_TOR_CTRL: &str = r#"
[Unit]
Description=Forward Port 9051 to host Tor socket
[Service]
ExecStart=socat TCP-LISTEN:9051,fork UNIX-CONNECT:/run/host/dumbcontainer-bridge/tor/control.sock
Type=simple
Restart=always
User=nobody
Group=nobody

NoNewPrivileges=true
ProtectSystem=true
ProtectHome=true
PrivateDevices=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
"#;

const AUTOLOGIN: &str = r#"
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --keep-baud --autologin amnesia - 115200,38400,9600 $TERM
"#;
