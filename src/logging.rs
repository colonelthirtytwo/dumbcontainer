use std::{
	fmt::Display,
	path::Path,
};

use anyhow::Context;
use fern::colors::ColoredLevelConfig;

pub fn setup_logging(dest: Option<&Path>, v: i32, q: i32) -> anyhow::Result<()> {
	let time_offset = time::UtcOffset::current_local_offset().unwrap_or(time::UtcOffset::UTC);
	let time_format = time::macros::format_description!("[year]:[month]:[day] [hour]:[minute]:[second].[subsecond digits:2][offset_hour sign:mandatory]:[offset_minute]:[offset_second]");

	let is_tty = dest.is_none() && atty::is(atty::Stream::Stderr);
	let colors = is_tty.then_some(
		ColoredLevelConfig::new()
			.error(fern::colors::Color::Red)
			.warn(fern::colors::Color::Yellow)
			.info(fern::colors::Color::Green)
			.debug(fern::colors::Color::Cyan)
			.trace(fern::colors::Color::BrightBlack),
	);

	let dispatch = fern::Dispatch::new()
		.format(move |out, message, record| {
			out.finish(format_args!(
				"[{}][{}][{}] {}{}",
				time::OffsetDateTime::now_utc()
					.to_offset(time_offset)
					.format(time_format)
					.unwrap(),
				ColorIfTty(colors.as_ref(), record.level()),
				record.target(),
				message,
				if is_tty { "\r" } else { "" },
			))
		})
		.level(match (v as i32) - (q as i32) {
			v if v <= -2 => log::LevelFilter::Error,
			v if v <= -1 => log::LevelFilter::Warn,
			0 => log::LevelFilter::Info,
			1 => log::LevelFilter::Debug,
			_ => log::LevelFilter::Trace,
		});
	let dispatch = if let Some(dest) = dest {
		let f = std::fs::File::create(dest).context("While opening log file")?;
		dispatch.chain(f)
	} else {
		dispatch.chain(std::io::stderr())
	};
	dispatch.apply().expect("Could not set up logging");
	Ok(())
}

struct ColorIfTty<'a>(Option<&'a ColoredLevelConfig>, log::Level);
impl<'a> Display for ColorIfTty<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		if let Some(colors) = self.0 {
			colors.color(self.1).fmt(f)
		} else {
			self.1.fmt(f)
		}
	}
}
