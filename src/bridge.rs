use std::path::{
	Path,
	PathBuf,
};

use anyhow::Context;
use nix::{
	mount::{
		MntFlags,
		MsFlags,
	},
	sys::stat::Mode,
};

pub struct BridgeDir {
	path: PathBuf,
	removed: bool,
}

macro_rules! impl_bridge_dirs {
	($ty:ident $mkdir:ident [$($dir:ident),* $(,)?]) => {
		impl $ty {
			$(
				#[allow(unused)]
				pub fn $dir(&self) -> PathBuf {
					self.path.join(stringify!($dir))
				}
			)*

			fn $mkdir(&self) -> anyhow::Result<()> {
				$({
					let path = self.path.join(stringify!($dir));
					nix::unistd::mkdir(&path, Mode::S_IRWXU)
						.with_context(|| format!("While creating dir {:?}", path))?;
				})*
				Ok(())
			}
		}
	};
}

impl BridgeDir {
	pub fn create(path: &Path) -> anyhow::Result<Self> {
		nix::unistd::mkdir(path, Mode::S_IRWXU)?;
		scopeguard::defer_on_unwind! {
			let _ = std::fs::remove_dir(&path);
		}

		nix::mount::mount(
			Some("dumbcontainer-bridge"),
			path,
			Some("tmpfs"),
			MsFlags::MS_RELATIME | MsFlags::MS_NOSUID | MsFlags::MS_NOEXEC,
			None as Option<&str>,
		)
		.with_context(|| format!("While mounting tmpfs on {:?}", &path))?;
		scopeguard::defer_on_unwind! {
			let _ = nix::mount::umount2(path, MntFlags::MNT_DETACH);
		}

		nix::mount::mount(
			None as Option<&str>,
			path,
			None as Option<&str>,
			MsFlags::MS_SHARED,
			None as Option<&str>,
		)
		.context("While making bridge tmpfs shared")?;

		let this = Self {
			path: path.into(),
			removed: false,
		};

		this.mkdirs()?;
		Ok(this)
	}

	pub fn _remove(mut self) -> anyhow::Result<()> {
		self.removed = true;
		self.drop_impl()
	}

	fn drop_impl(&self) -> anyhow::Result<()> {
		nix::mount::umount2(&self.path, MntFlags::MNT_DETACH)
			.with_context(|| format!("While unmounting bridge tmpfs {:?}", self.path))?;
		std::fs::remove_dir(&self.path)
			.with_context(|| format!("While removing bridge fs {:?}", self.path))?;
		Ok(())
	}
}
impl std::ops::Drop for BridgeDir {
	fn drop(&mut self) {
		if !self.removed {
			if let Err(e) = self.drop_impl() {
				error!("Could not remove bridge dir: {:#}", e);
			}
		}
	}
}

impl_bridge_dirs!(BridgeDir mkdirs [
	tor,
	//xpra,
]);
