#[macro_use]
extern crate log;

mod bridge;
mod cryptsetup;
mod dbus;
mod install;
mod logging;
mod mount;
mod pidfd;
mod secbpf;
mod secbpfsrc;
mod tor;
mod util;

use std::{
	os::unix::prelude::{
		AsRawFd,
		FromRawFd,
	},
	path::{
		Path,
		PathBuf,
	},
};

use anyhow::Context;
use nix::{
	poll::{
		PollFd,
		PollFlags,
	},
	sys::stat::Mode,
};

use crate::util::cargs;

fn main() {
	let args = <Args as clap::Parser>::parse();
	if let Err(e) = logging::setup_logging(
		args.log_file.as_deref(),
		args.verbose as i32,
		args.quiet as i32,
	) {
		eprintln!("{:#}", e);
		std::process::exit(1);
	}

	if let Err(e) = app_main(args) {
		log::error!("{:#}", e);
		std::process::exit(1);
	}
}

#[derive(clap::Parser)]
pub struct Args {
	#[clap(subcommand)]
	pub subcommand: Subcommands,

	/// Log more. Can be specified multiple times
	#[clap(short = 'v', long, action(clap::ArgAction::Count))]
	pub verbose: u8,
	/// Log less. Can be specified multiple times
	#[clap(short = 'q', long, action(clap::ArgAction::Count))]
	pub quiet: u8,
	/// File to write log to. If not set, write to stderr
	#[clap(short = 'L', long, parse(from_os_str))]
	pub log_file: Option<PathBuf>,
}

#[derive(clap::Subcommand)]
pub enum Subcommands {
	/// Starts a system
	Start(StartArgs),
	/// Formats and installs a new system
	Install(InstallArgs),
}

#[derive(clap::Args)]
pub struct StartArgs {
	/// Block device or image containing the encrypted container partition
	pub encrypted_partition: PathBuf,

	/// Target to boot into. 'init' (default) to start systemd.
	/// 'shell' to start /usr/bin/sh.
	#[clap(short = 't', long, default_value = "init")]
	pub target: Target,

	/// Overlay host's pacman cache under guests, letting the guest to use host cache.
	#[clap(long)]
	pub share_paccache: bool,
}

#[derive(clap::Args)]
pub struct InstallArgs {
	/// Block device or image to format and install a system in
	pub encrypted_partition: PathBuf,
}

pub enum Target {
	Init,
	Shell,
}
impl std::str::FromStr for Target {
	type Err = String;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"init" => Ok(Self::Init),
			"shell" => Ok(Self::Shell),
			_ => Err(format!("Unrecognized target: {:?}", s)),
		}
	}
}

pub fn app_main(args: Args) -> anyhow::Result<()> {
	// Ignore SIGPIPE, get EOF instead please
	nix::sys::signal::sigprocmask(
		nix::sys::signal::SigmaskHow::SIG_BLOCK,
		Some(&{
			let mut set = nix::sys::signal::SigSet::empty();
			set.add(nix::sys::signal::SIGPIPE);
			set
		}),
		None,
	)
	.context("While setting up signals")?;

	match args.subcommand {
		Subcommands::Start(args) => start_main(args),
		Subcommands::Install(args) => install::setup(args),
	}
}

fn start_main(args: StartArgs) -> anyhow::Result<()> {
	// Make a unique id, for use in container names, etc.
	let id = crate::util::generate_random_id();
	debug!("Container temporary id: {}", id);

	crate::mount::with_mounted_container(
		&args.encrypted_partition,
		&id,
		None,
		|| Ok(()),
		|| {
			let mut tor_pidfd = Some(tor::start_tor().context("While starting tor")?);
			let container_pidfd = exec_container(&args).context("While starting container")?;

			// Wait loop
			let mut pollfds = vec![];
			'main: loop {
				pollfds.clear();
				if let Some(ref tor_pidfd) = tor_pidfd {
					pollfds.push(PollFd::new(tor_pidfd.as_raw_fd(), PollFlags::POLLIN));
				}
				pollfds.push(PollFd::new(container_pidfd.as_raw_fd(), PollFlags::POLLIN));

				nix::poll::poll(&mut pollfds, -1).context("While polling")?;
				for pollfd in pollfds.drain(..) {
					if pollfd.revents().unwrap().is_empty() {
						continue;
					}

					if pollfd.as_raw_fd() == container_pidfd.as_raw_fd() {
						break 'main;
					}
					if let Some(ref ator_pidfd) = tor_pidfd {
						if pollfd.as_raw_fd() == ator_pidfd.as_raw_fd() {
							// TODO: restart?
							warn!("Tor exited unexpectedly, internet will not be available");
							ator_pidfd.wait().context("While waiting for tor")?;
							std::mem::drop(tor_pidfd.take());
						}
					}
				}
			}

			let _ = container_pidfd.signal(nix::libc::SIGTERM);
			if let Err(e) = container_pidfd.wait_checked("systemd-nspawn") {
				warn!("{:?}", e);
			}

			if let Some(tor_pidfd) = tor_pidfd {
				let _ = tor_pidfd.signal(nix::libc::SIGTERM);
				if let Err(e) = tor_pidfd.wait_checked("tor") {
					warn!("{:?}", e);
				}
			}

			Ok(())
		},
	)
}

fn exec_container(args: &StartArgs) -> anyhow::Result<pidfd::Pidfd> {
	let mut pidfd = 0;
	let mut clone = clone3::Clone3::default();
	clone
		.exit_signal(nix::libc::SIGCHLD as u64)
		.flag_vfork()
		.flag_pidfd(&mut pidfd);
	let pid = unsafe { clone.call() }.context("While cloning systemd-nspawn")?;
	if pid == 0 {
		util::cloned_child(|| child_container(args));
	}

	Ok(pidfd::Pidfd::from_fd(pidfd))
}

fn child_container(args: &StartArgs) -> anyhow::Result<()> {
	unsafe { nix::libc::prctl(nix::libc::PR_SET_PDEATHSIG, nix::libc::SIGTERM) };

	let mut cargs = Vec::from(cargs![
		"/usr/bin/systemd-nspawn",
		"--settings=false",
		"--private-users=identity",
		"--private-network",
		"--register=no",
		"--keep-unit",
		"--machine=dumbcontainer",
		"--bind=/tmp/dumbcontainer-bridge:/run/host/dumbcontainer-bridge",
		"--resolv-conf=off",
		"--link-journal=no",
		"--timezone=off",
		"-D",
		"/tmp/dumbcontainer-mount/root",
	]);

	match args.target {
		Target::Init => {
			cargs.extend_from_slice(&cargs![
				"-b",
				"--",
				"systemd.wants=socat-tor-proxy.service",
				"systemd.wants=socat-tor-ctrl.service"
			]);
		}
		Target::Shell => {}
	}

	let env: &[&std::ffi::CStr] = &[];

	nix::unistd::execvpe(&cargs[0], &cargs, env).context("While executing systemd-nspawn")?;
	Ok(())
}

#[allow(unused)]
fn debug_fork_shell(pty_path: Option<&Path>) -> anyhow::Result<()> {
	use std::process::Stdio;
	let (stdin, stdout, stderr) = if let Some(pty_path) = pty_path {
		let fd = nix::fcntl::open(
			pty_path,
			nix::fcntl::OFlag::O_RDWR | nix::fcntl::OFlag::O_CLOEXEC | nix::fcntl::OFlag::O_NOCTTY,
			Mode::empty(),
		)?;
		unsafe {
			(
				Stdio::from_raw_fd(fd),
				Stdio::from_raw_fd(fd),
				Stdio::from_raw_fd(fd),
			)
		}
	} else {
		(Stdio::inherit(), Stdio::inherit(), Stdio::inherit())
	};

	std::process::Command::new("/usr/bin/bash")
		.stdin(stdin)
		.stdout(stdout)
		.stderr(stderr)
		.spawn()?
		.wait()?;

	Ok(())
}
