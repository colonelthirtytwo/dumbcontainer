use std::{
	os::unix::prelude::AsRawFd,
	path::Path,
};

use anyhow::Context;
use nix::{
	mount::MsFlags,
	sys::stat::Mode,
};

use crate::util::ProcExt;

pub fn with_mounted_container<R, BMF, AMF>(
	encrypted_partition: &Path,
	id: &str,
	passphrase: Option<&str>,
	before_mount_func: BMF,
	after_mount_func: AMF,
) -> anyhow::Result<R>
where
	BMF: FnOnce() -> anyhow::Result<()>,
	AMF: FnOnce() -> anyhow::Result<R>,
{
	debug!("Unsharing mount namespace");
	nix::sched::unshare(nix::sched::CloneFlags::CLONE_NEWNS)
		.context("While unsharing mount namespace")?;
	nix::mount::mount(
		None as Option<&str>,
		"/",
		None as Option<&str>,
		MsFlags::MS_SLAVE | MsFlags::MS_REC,
		None as Option<&str>,
	)
	.context("While unsharing mounts")?;

	// Make our own secret /tmp
	nix::mount::mount(
		Some("tmpfs"),
		"/tmp",
		Some("tmpfs"),
		MsFlags::MS_RELATIME,
		None as Option<&str>,
	)
	.context("While mounting tmpfs")?;

	debug!("Unsharing mount namespace");
	nix::sched::unshare(nix::sched::CloneFlags::CLONE_NEWNS)
		.context("While unsharing mount namespace")?;
	nix::mount::mount(
		None as Option<&str>,
		"/",
		None as Option<&str>,
		MsFlags::MS_SLAVE | MsFlags::MS_REC,
		None as Option<&str>,
	)
	.context("While unsharing mounts")?;

	// Make our own secret /tmp
	nix::mount::mount(
		Some("tmpfs"),
		"/tmp",
		Some("tmpfs"),
		MsFlags::MS_RELATIME,
		None as Option<&str>,
	)
	.context("While mounting tmpfs")?;

	debug!("Setting up bridge");
	let _bridge_dir = crate::bridge::BridgeDir::create("/tmp/dumbcontainer-bridge".as_ref())
		.context("While setting up bridge directory")?;

	let systemd = crate::dbus::SystemD::new().context("While connecting to dbus")?;
	let scope = systemd
		.create_scope(
			&format!("dumbcontainer_{}.scope", id),
			false,
			vec![nix::unistd::getpid().as_raw() as u32],
		)
		.context("While creating scope unit")?;
	let cgroup_fd = scope.open().context("While opening cgroup")?;

	// Start LSM BPF. Prevents outside access to the unencrypted partition.
	// Do before opening the encrypted partition, otherwise race condition.
	debug!("Setting up LSM eBPF");
	let mut bpf =
		crate::secbpf::SecBpf::new(cgroup_fd.as_raw_fd()).context("While setting up BPF")?;

	debug!("Creating DM device");
	let mut cs = crate::cryptsetup::ContainerStorage::create_uninitialized(
		&format!("dumbcontainer-{}", id),
		id,
		Some(&mut bpf),
	)?;

	debug!("Opening encrypted partition");
	cs.decrypt(encrypted_partition, passphrase)?;
	cs.mknod(
		"/tmp/dumbcontainer-plaintext-dev".as_ref(),
		Mode::from_bits_truncate(0o600),
	)
	.context("While creating plaintext device node")?;

	(before_mount_func)()?;

	debug!("Mounting partition");
	std::fs::create_dir("/tmp/dumbcontainer-mount")
		.context("While creating /tmp/dumbcontainer-mount")?;
	std::process::Command::new("mount")
		.args(&[
			"-o",
			"relatime",
			"/tmp/dumbcontainer-plaintext-dev",
			"/tmp/dumbcontainer-mount",
		])
		.spawn()
		.context("While executing mount")?
		.wait_checked("mount")?;

	let res = (after_mount_func)()?;

	// Cleanup even in an error, but don't squelch the inner error
	cs.delete().context("While closing encrypted partition")?;

	Ok(res)
}
