use std::{
	convert::TryInto,
	os::unix::io::RawFd,
	path::Path,
};

use anyhow::Context;
use nix::libc::dev_t;

use crate::secbpfsrc::{
	SecSkel,
	SecSkelBuilder,
};

// TODO: pin program to prevent unloading when process exits

pub struct SecBpf {
	_opened: SecSkel<'static>,
}
impl SecBpf {
	pub fn new(cgroup_fd: RawFd) -> anyhow::Result<Self> {
		let builder = SecSkelBuilder::default();
		let open_skel = builder.open()?;
		let mut skel = open_skel.load()?;

		let key_bytes = 0u32.to_ne_bytes();
		let value_bytes = cgroup_fd.to_ne_bytes();
		skel.maps_mut().disk_authorized_cgroup_map().update(
			&key_bytes,
			&value_bytes,
			libbpf_rs::MapFlags::ANY,
		)?;

		skel.attach()?;

		Ok(Self { _opened: skel })
	}

	pub fn set_plaintext_dev(&mut self, dev: Option<dev_t>) {
		self._opened.bss().plaintext_dev = dev.unwrap_or(0).try_into().unwrap();
	}

	pub fn pin(&mut self, prefix: &str) -> anyhow::Result<()> {
		self._opened
			.progs_mut()
			.handle_mount()
			.pin(Path::new("/sys/fs/bpf/").join(format!("dumbcontainer-{}-handle_mount", prefix)))
			.context("While pinning handle_mount program")?;
		self._opened
			.progs_mut()
			.handle_inode_perms()
			.pin(
				Path::new("/sys/fs/bpf/")
					.join(format!("dumbcontainer-{}-handle_inode_perms", prefix)),
			)
			.context("While pinning handle_inode_perms program")?;
		Ok(())
	}

	pub fn unpin(&mut self, prefix: &str) -> anyhow::Result<()> {
		self._opened
			.progs_mut()
			.handle_mount()
			.unpin(Path::new("/sys/fs/bpf/").join(format!("dumbcontainer-{}-handle_mount", prefix)))
			.context("While unpinning handle_mount program")?;
		self._opened
			.progs_mut()
			.handle_inode_perms()
			.unpin(
				Path::new("/sys/fs/bpf/")
					.join(format!("dumbcontainer-{}-handle_inode_perms", prefix)),
			)
			.context("While unpinning handle_inode_perms program")?;
		Ok(())
	}
}
