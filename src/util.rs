use anyhow::Context;
use nix::mount::MsFlags;
use rand::RngCore;

macro_rules!cargs{
	($($lit:literal),* $(,)?) => {
		[
			$(
				::std::ffi::CStr::from_bytes_with_nul(::std::concat!($lit, "\0").as_bytes()).unwrap()
			),*
		]
	};
}
pub(crate) use cargs;

pub(crate) fn cloned_child<F: FnOnce() -> anyhow::Result<()>>(f: F) -> ! {
	if let Err(e) = (f)() {
		error!("{:?}", e);
		std::process::exit(1);
	}
	error!("Child should have execve'd but didn't");
	unsafe { nix::libc::abort() };
}

pub(crate) fn generate_random_id() -> String {
	let mut rng_buf = [0u8; 8];
	rand::rngs::OsRng.fill_bytes(&mut rng_buf);
	let mut hex_buf = [0u8; 16];
	hex::encode_to_slice(&rng_buf, &mut hex_buf).unwrap();
	String::from(std::str::from_utf8(&hex_buf).unwrap())
}

pub(crate) fn unshare_mounts(mount_type: MsFlags) -> anyhow::Result<()> {
	nix::sched::unshare(nix::sched::CloneFlags::CLONE_NEWNS)
		.context("While unsharing mount namespace")?;
	// Unshare mounts. See also Notes section in man 7 mount_namespaces
	nix::mount::mount(
		None as Option<&str>,
		"/",
		None as Option<&str>,
		mount_type | MsFlags::MS_REC,
		None as Option<&str>,
	)
	.context("While unsharing mounts")?;
	Ok(())
}

pub(crate) trait ProcExt {
	/// Waits for process and also checks the return code
	fn wait_checked(&mut self, desc: &str) -> anyhow::Result<()>;
}
impl ProcExt for std::process::Child {
	fn wait_checked(&mut self, desc: &str) -> anyhow::Result<()> {
		let es = self
			.wait()
			.with_context(|| format!("While waiting on process {}", desc))?;
		if !es.success() {
			anyhow::bail!("Process {} exited with code {}", desc, es);
		}
		Ok(())
	}
}

pub(crate) trait CmdExt {
	fn spawn_wait_check(&mut self, desc: &str) -> anyhow::Result<()>;
	fn output_checked(&mut self, desc: &str) -> anyhow::Result<std::process::Output>;
}
impl CmdExt for std::process::Command {
	fn spawn_wait_check(&mut self, desc: &str) -> anyhow::Result<()> {
		self.spawn()
			.with_context(|| format!("While executing {}", desc))?
			.wait_checked(desc)
	}

	fn output_checked(&mut self, desc: &str) -> anyhow::Result<std::process::Output> {
		let out = self
			.output()
			.with_context(|| format!("While executing {}", desc))?;
		if !out.status.success() {
			let mut err = anyhow::anyhow!("Process {} exited with code {}", desc, out.status);
			if !out.stdout.is_empty() {
				err = err.context(format!(
					"stdout: {:?}",
					String::from_utf8_lossy(&out.stdout)
				));
			}
			if !out.stderr.is_empty() {
				err = err.context(format!(
					"stderr: {:?}",
					String::from_utf8_lossy(&out.stderr)
				));
			}
			return Err(err);
		}
		Ok(out)
	}
}
